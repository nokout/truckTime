# truckTime
Track and check working time for truck drivers


##Rules Expected To Be Supported
Based on the https://www.nhvr.gov.au/files/nhvr-national-driver-work-diary-08-2013.pdf document
-------------|--------------|---------------|---------------------------|------------------------------|--------------------|---------------|----------------
5.5 hours|5.25      |15 continuous minutes rest time    |TRUE|TRUE|TRUE||
8 hours|7.5     |2 * 15 continuous minutes rest time    |TRUE|TRUE|TRUE||
11 hours|10     |4 * 15 continuous minutes rest time    |TRUE|TRUE|TRUE||
24 hours|12     |7 continuous hours stationary      |TRUE|FALSE|TRUE||
24 hours        |12     |5 continuous hours rest time sleeper/stationary  |FALSE|TRUE|FALSE||
52 hours        |n/a        |10 continuous hours stationary rest    |FALSE|TRUE|FALSE||
7 days|72       |24 contiuous hours stationary      |TRUE|FALSE|FALSE||
7 days          |n/a        |6 x night rest breaks          |FALSE|FALSE|TRUE||
7 days          |60 hours       |24 cont hours AND 24 hours stationary rest time where blocks > 7 |FALSE|TRUE|FALSE||
14 days |144        |2* night AND 2*night cons      |TRUE|FALSE|FALSE||
14 days     |120        |2* night AND 2*night cons      |FALSE|TRUE|FALSE||
28 days     |288        |4 x 24 continuous hours        |FALSE|TRUE|FALSE||

##TODO: Implement BFM Rules