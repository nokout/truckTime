/*RuleSet:  Standard Hours
-------------------- --------------------------------------------------   --------------------------
work hours	rest type 				max distance(time)
-------------------- --------------------------------------------------   --------------------------
5.25		15 continuous minutes rest time 	5.5 hours
7.5		2 * 15 continuous minutes rest time 	8 hours
10		4 * 15 continuous minutes rest time 	11 hours
12		7 continuous hours stationary 		24 hours
12		5 continuous hours rest 		24 hours 		(Two-up drivers)
-		    time sleeper/stationary		-
n/a 		10 continuous hours stationary rest 	52 hours  		(Two-up drivers)
72		24 contiuous hours stationary 		7 days
n/a		6 x night rest breaks 			7 days			(Fatigue-regulated bus and coach drivers)
60 hours   	24 cont hours AND 24 hours 		7 days			(Two-up drivers)
-		stationary rest time where blocks > 7	-
144		2* night AND 2*night cons 		14 days
120		2* night AND 2*night cons 		14 days		(Two-up drivers)
288		4 x 24 continuous hours 		28 days		(Fatigue-regulated bus and coach drivers)

RuleSet: Basic Fatigue Management

AFM (Could let them BYO rules)
*/
var assert = require("assert");
var rules = require('../app/rules/timeRecords');
var moment = require('moment');


var breaks = [];

var testOns = [{
  start: moment().subtract(16, 'hours'),
  end: moment().subtract(8, 'hours'),
  comment: 'later'
}, {
  start: moment().subtract(24, 'hours'),
  end: moment().subtract(18, 'hours'),
  comment: 'earlier'
}];

describe('truckTime Rules Testing', function() {

  describe('Check Testing Works', function() {
    it('Array equal returns -1 if not found', function() {
      assert.equal(-1, [1, 2, 3].indexOf(5));
      assert.equal(-1, [1, 2, 3].indexOf(0));
    });
  });

  before(function() {
    // runs before all tests in this block
    rules.sortPeriodArray(testOns);
    breaks = rules.buildBreaks(testOns);
  });

  describe('Simple Two Period Case', function() {
    describe('Period Management', function() {
      it('Sorted periods are in order', function() {
        assert.equal('earlier', testOns[0].comment);
        assert.equal('later', testOns[1].comment);
      });
      it('Breaks fill remaining time', function() {
        assert.equal(moment(breaks[0].start).format(), moment(testOns[0].end).format());
        assert.equal(breaks[0].end, testOns[1].start);
      });
    });
    describe('Rule Application', function() {
      describe('Breaks', function() {
        it('<5.25hrs work && 1x 15min within 5.5hrs', function() {
          assert.ok(false);
        });
        it('<7.5hrs work && 2x 15min breaks within 8hrs', function() {
          assert.ok(false);
        });
        it('<10hrs work &&  4x 15min breaks within 11hrs', function() {
          assert.ok(false);
        });
        it('<12hrs work && 1x 7hr break within 24hrs', function() {
          assert.ok(false);
        });
        it('<12hrs work && 1x 5hr within 24hrs', function() {
          assert.ok(false);
        });
        it('<60hrs work && 1x 24hr break within 7days', function() {
          assert.ok(false);
        });
        it('1x 10hr break wihtin 52hrs (Two Up)', function() {
          assert.ok(false);
        });
        it('<72hrs	work && 24 contiuous hours stationary within 7 days', function() {
          assert.ok(false);
        });
        it('6 x night rest breaks within 7 days (Fatigue-regulated bus and coach drivers)', function() {
          assert.ok(false);
        });
        it('<60hrs work && 24 cont hours PLUS 24 hours within 7 days (Two-up drivers) -	stationary rest time where blocks > 7	-', function() {
          assert.ok(false);
        });
        it('<144hrs work &&  2* night PLUS 2x night cons within 14 days', function() {
          assert.ok(false);
        });
        it('<288hrs work && 4x x 24 continuous hour breaks within 28 days		(Fatigue-regulated bus and coach drivers)', function() {
          assert.ok(false);
        });
      });
    });

    /*RuleSet:  Standard Hours
    -------------------- --------------------------------------------------   --------------------------
    work hours	rest type 				max distance(time)
    -------------------- --------------------------------------------------   -------------------------
    144		2* night AND 2*night cons 		14 days
    120		2* night AND 2*night cons 		14 days		(Two-up drivers)
    288		4 x 24 continuous hours 		28 days		(Fatigue-regulated bus and coach drivers)

    RuleSet: Basic Fatigue Management

    AFM (Could let them BYO rules)
    */


  });
});
