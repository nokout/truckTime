var moment = require('moment');

//NOTE: Assume that current period will extend forever. If it is rest then we can calc when driver can restart, if work, then we can tell when/what next break is required.

//NOTE: Events clock tick, period change

//this is actually a schema??
var period = {
  start: null,
  end: null,
  durationMinutes: null,
  comment: null,
  lastMajorBreakFinishTime: null
};

workTimes = {
  ons: [],
  offs: []
};
module.exports = {
  sortPeriodArray: function(array) {
    return array.sort(function(left, right) {
      return left.start - right.start;
    });
  },
  buildBreaks: function(shifts) {
    var breaks = [];
    if (shifts.length > 1) {
      shifts.forEach(function(period, index, array) {
        if (index !== (array.length - 1)) {
          // console.log(JSON.stringify(array));
          // console.log(index+1);
          // console.log('nxt start' + moment(array[index + 1].start).format());
          breakPeriod = {
            start: period.end,
            end: array[index + 1].start
          };
          breaks.push(breakPeriod);
        }
      });
    }
    return breaks;

  }


};



//TODO: Miniumum Previous Time  To Understand
//TODO: Deal with timezone changes
//NOTE: rules are defined at: https://www.nhvr.gov.au/files/nhvr-national-driver-work-diary-08-2013.pdf

//rest break types
/*
// at the end of any given work period the followup must be met
// check for now and display diff between actual distance and allowed distance, giving time remaining, can then display the one with the shortest time remaining. (and others if preferenced)

RuleSet:  Standard Hours
-------------------- --------------------------------------------------   --------------------------
work hours	rest type 				max distance(time)
-------------------- --------------------------------------------------   --------------------------
5.25		15 continuous minutes rest time 	5.5 hours
7.5		2 * 15 continuous minutes rest time 	8 hours
10		4 * 15 continuous minutes rest time 	11 hours
12		7 continuous hours stationary 		24 hours
12		5 continuous hours rest 		24 hours 		(Two-up drivers)
-		    time sleeper/stationary		-
n/a 		10 continuous hours stationary rest 	52 hours  		(Two-up drivers)
72		24 contiuous hours stationary 		7 days
n/a		6 x night rest breaks 			7 days			(Fatigue-regulated bus and coach drivers)
60 hours   	24 cont hours AND 24 hours 		7 days			(Two-up drivers)
-		stationary rest time where blocks > 7	-
144		2* night AND 2*night cons 		14 days
120		2* night AND 2*night cons 		14 days		(Two-up drivers)
288		4 x 24 continuous hours 		28 days		(Fatigue-regulated bus and coach drivers)

RuleSet: Basic Fatigue Management

AFM (Could let them BYO rules)


*/
thingexports = {
  fakeHours: function() {},
  majorRestBreaks: [],
  workPeriods: [],

  addWorkPeriod: function(workPeriod) {},

  removeWorkPeriod: function(workPeriod) {},

  areLongBreakRulesNeeded: function() {},

  shrinkToFit: function() {},

  expandToFill: function() {},

  get24HourRestBreakEnd: function() {
    /*
	24-hour periods should be counted from the end of the following rest breaks:
		standard hours solo – 7 or more continuous hours
		standard hours two-up – 5 or more continuous hours
		BFM hours solo – 7 or more continuous hours (or from the end of a rest break of 6 continuous hours if a ‘split rest break’ has been taken)
		BFM hours two-up – any rest break (as this option does not require a majorrest break in a 24-hour period)
		AFM hours – the required relevant major rest break under the AFM  accreditation (as stated on the AFM accreditation certificate).
*/

  },


  /*
            // Periods of less than 24 hours should be counted forward from the end of any rest break (see page 25 for an example).
            // This rule ensures that drivers have their required short rest breaks (for example, a standard hours solo driver must not work for more than 5¼ hours in any period of 5½ hours).
            function periodLessThan24Hours() {
                //????
            },

            //Periods of 24 hours or longer, including periods of 7, 14 or 28 days, should be counted forward from the end of the major rest break relevant to the period in your hours option. To see which major rest break is relevant to a period, look in the ‘Rest’ column in the table for your work and rest hours option on page 27 or 28.
            function periodGreaterThan24Hours: function() {
                //????
            }

*/

  rules: {
    /*	use15MinIncrements: function(){
		var result = false;
		this.workPeriods.forEach(function(period){
			// period.calculatedTimeOn is divisible by 15 and period.calculatedTimeOn is divisible by 15
		});
	},
*/
    //Work time limits are maximum limits, so work time is always rounded up to the next 15 minute interval. For example:
    general: [

      function roundUpWorkTime() {
        //(end - start).minutes % 15 = 0
      },

      //Rest time limits are minimum limits, so rest time is always rounded down to the last 15 minute interval. For example
      function roundDownRestTime() {
        //(end - start).minutes % 15 = 0
      },

      //Drivers travelling outside their normal time zone must ensure that they count and record time, including night rests, according to the time zone of their base
      function timeIsForBase() {
        //(end - start).minutes % 15 = 0
      },

    ],

    shortBreak: [

    ],
    majorBreaks: [

    ]
  }
};
